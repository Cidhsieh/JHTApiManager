import Foundation

@available(iOS 13.0, *)
@available(macOS 10.15, *)

open class BasicAPIViewModel: ObservableObject, ErrorMessageHandler {
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    
    public init() {}
    
    public func startLoading() {
        DispatchQueue.main.async {
            self.isLoading = true
        }
    }
    public func endLoading(_ completion: @escaping ()->Void) {
        DispatchQueue.main.async {
            self.isLoading = false
            completion()
        }
    }
    
    open func resetState() {
        errorMessage = ""
        isLoading = false
    }
}


protocol ErrorMessageHandler {
    
}
