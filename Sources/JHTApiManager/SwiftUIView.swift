//
//  SwiftUIView.swift
//  JHTChart
//
//  Created by cidhsieh on 2021/11/8.
//

import SwiftUI

@available(iOS 13.0, *)
@available(macOS 10.15, *)
public struct SwiftUIView: View {
    public init(){}
    public var body: some View {
        Text("Hello, World!")
    }
}

@available(iOS 13.0, *)
@available(macOS 10.15, *)
struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
