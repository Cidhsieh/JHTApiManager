import XCTest
@testable import JHTApiManager

final class JHTApiManagerTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(JHTApiManager().text, "Hello, World!")
    }
}
